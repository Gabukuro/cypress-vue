describe('Teste de valores nulos', function(){
   
    it('Teste de valores nulos', function() {
        cy.visit('http://localhost:8080/');
        cy.get('#ladoA').type(0);
        cy.get('#ladoB').type(0);
        cy.get('#ladoC').type(0);
        cy.get('.btn').click();
    });


    it('Teste de triângulos inexistentes', function(){

        cy.fixture('inexistentes').then((inexistentes) => {

            inexistentes.forEach(element => {
                cy.get('#ladoA').clear().type(element[0]);
                cy.get('#ladoB').clear().type(element[1]);
                cy.get('#ladoC').clear().type(element[2]);
                cy.get('.btn').click();
                cy.get('p').should('contain', 'Triângulo inexistente !');
            });

        });
    });


    it('Teste de triângulos equiláteros', function(){

        cy.fixture('equilateros').then((equilateros) => {

            equilateros.forEach(element => {
                cy.get('#ladoA').clear().type(element[0]);
                cy.get('#ladoB').clear().type(element[1]);
                cy.get('#ladoC').clear().type(element[2]);
                cy.get('.btn').click();
                cy.get('p').should('contain', 'Triângulo equilátero !');
                
            });

        });
    });

    it('Teste de triângulos escalenos', function(){
        
        cy.fixture('escalenos').then((escalenos) => {
            
            escalenos.forEach(element => {
    
                cy.get('#ladoA').clear().type(element[0]);
                cy.get('#ladoB').clear().type(element[1]);
                cy.get('#ladoC').clear().type(element[2]);
                cy.get('.btn').click();
                cy.get('p').should('contain', 'Triângulo escaleno !');
                
            });
        
        });
    });

    
    it('Teste de triângulos isósceles', function() {

        cy.fixture('isosceles').then((isosceles) => {
            
            isosceles.forEach(element => {
                
                cy.get('#ladoA').clear().type(element[0]);
                cy.get('#ladoB').clear().type(element[1]);
                cy.get('#ladoC').clear().type(element[2]);
                cy.get('.btn').click();
                cy.get('p').should('contain', 'Triângulo isósceles !');
                
            });
        
        });

    });
});